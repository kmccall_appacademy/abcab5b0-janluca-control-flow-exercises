# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.each do |letter|
    if letter = letter.downcase
      str.delete!(letter)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    first_letter = (str.length / 2) - 1
    second_letter = (str.length / 2)
    str.split("")[first_letter..second_letter].join
  else
    letter = (str.length - 1) / 2
    str.split("")[letter]
  end
end


# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars.each do |letter|
    if VOWELS.include?(letter)
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 1
  idx = 1
  num.times do
    factorial *= idx
    idx += 1
  end
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each_with_index do |letter, ind|
    if ind != arr.length - 1
      joined += (letter + separator)
    else
      joined += letter
    end
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcased = ""
  str.chars.each_with_index do |letter, ind|
    if ind % 2 == 0
      weirdcased += letter.downcase
    else
      weirdcased += letter.upcase
    end
  end
  weirdcased
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed = []
  str.split.map! do |word|
    if word.length >= 5
      reversed << word.reverse
    else
      reversed << word
    end
  end
  reversed.join(" ")
end


# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  idx = 1
  array = []
  until idx > n
    if idx % 3 == 0 && idx % 5 == 0
      array << "fizzbuzz"
    elsif idx % 3 == 0
      array << "fizz"
    elsif idx % 5 == 0
      array << "buzz"
    else
      array << idx
    end
    idx += 1
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  idx = 2
  while idx < num
    if num % idx == 0
      return false
    end
    idx += 1
  end
  true
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  idx = 1
  factors = []
  while idx <= num
    if num % idx == 0
      factors << idx
    end
    idx += 1
  end
  factors.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors(num).map! do |factor|
    if prime?(factor)
      prime_factors << factor
    end
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = arr.select { |num| num % 2 == 0}
  odds = arr.select { |num| num % 2 != 0}
  if odds.length > evens.length
    evens[0]
  else
    odds[0]
  end
end
